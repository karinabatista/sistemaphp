<?php
	include ("config/DB.class.php");
?>

<?php
	session_start();
	if(!isset($_SESSION["user"]) || !isset($_SESSION["password"])) {
		header("location: index.php#banner");
		exit;	
	} else {
		echo "Você está logado! Caso queira fazer logout, clique no botão Sair!";
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
	<head>
		<title>Painel Administrativo</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.onvisible.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">

		<!-- Header -->
			<div id="header">
				
				<!-- Nav -->
					<nav id="nav">
						<ul>
								<li><a href="">Ações</a>
									<ul>
										<li><a href="cadastro.php">Adicionar novo funcionário</a></li>
										<li><a href="listar.php">Listar Funcionários</a></li>
										<li><a href="listar.php">Editar Funcionários</a></li>
										<li><a href="listar.php">Excluir Funcionários</a></li>
									</ul>
								</li>
							<li><a href="logout.php">Sair</a></li>
						</ul>
					</nav>
			</div>

			<div class="wrapper style1">
				<div class="container">
					<article id="main" class="special">
						<header>
							<h2>Bem vindo!</h2>
							<p>	O que deseja fazer? </p>
							<p><a class="button circled scrolly" href="cadastro.php">Add novo funcionário</a>
							 <a class="button circled scrolly" href="listar.php" >Listar funcionários</a></p>
							<p><a class="button circled scrolly" href="listar.php">Editar funcionários</a>
							 <a class="button circled scrolly" href="listar.php">Excluir funcionários</a></p>
						</header>
					 </article>	
				</div>
			</div>					
			<div align="center" class="copyright">
				<ul>
					<li>Aplicativo Web | By: <a href="#">Karina Batista</a> | TIM: (31)9119-4889 | VIVO: (31)71409-0222</li>
				</ul>
			</div>
	</body>
</html>