<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
	<head>
		<title>Excluir Funcionários</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.onvisible.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body class="no-sidebar">

		<!-- Header -->
			<div id="header">
				
				<!-- Nav -->
					<nav id="nav">
						<ul>
								<li><a href="">Ações</a>
									<ul>
										<li><a href="cadastro.php">Adicionar novo funcionário</a></li>
										<li><a href="listar.php">Listar Funcionários</a></li>
										<li><a href="listar.php">Editar Funcionários</a></li>
										<li><a href="listar.php">Excluir Funcionários</a></li>
									</ul>
								</li>
							<li><a href="logout.php">Sair</a></li>
						</ul>
					</nav>
			</div>

			<div class="wrapper style1">
				<div class="container">
					<article id="main" class="special">
						<header>
							<?php
   								 include "config/DB.class.php";
	
								 $id = $_GET["id"];
	
								 if(mysql_query("DELETE FROM usuarios WHERE ID='$id'")) {
									echo "<h2>Funcionário removido com sucesso!</h2><br />";
									echo "<h4><p><center>Deseja excluir outro funcionário?</center></h4></p>";
									echo "<p><a href='listar.php'>Sim</a></p>";
									echo "<p><a href='painel.php'>Não</a></p>";
								} else {
									echo mysql_error();
								}
							?>
						</header>
					</article>		
				</div>
			</div>
		</body>
</html>				