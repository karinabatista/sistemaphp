-- phpMyAdmin SQL Dump
-- version 4.3.13
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 09-Abr-2015 às 04:10
-- Versão do servidor: 5.6.23
-- PHP Version: 5.4.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sistemaphp`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID` int(4) NOT NULL,
  `Usuario` varchar(10) NOT NULL,
  `Senha` varchar(6) NOT NULL,
  `nome` varchar(7) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `setor` varchar(14) DEFAULT NULL,
  `cargo` varchar(30) DEFAULT NULL,
  `foto` varchar(4) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`ID`, `Usuario`, `Senha`, `nome`, `email`, `setor`, `cargo`, `foto`) VALUES
(1, 'Pedro', '123121', 'Pedro', 'pedro@dj.emp.br', 'TI', 'Gerente', NULL),
(2, 'Joao', '123122', 'Joï¿½o', 'joao@dj.emp.br', 'TI', 'Programador', NULL),
(3, 'Flavio', '123123', 'Flávio', 'flavio@dj.emp.br', 'TI', 'Estagiário', NULL),
(4, 'Maria', '123124', 'Maria', 'maria@dj.emp.br', 'Administrativo', 'Secretï¿½ria', NULL),
(5, 'Amanda', '123125', 'Amanda', 'amanda@dj.emp.br', 'Administrativo', 'Estagiária', NULL),
(6, 'Manoel', '123126', 'Manoel', 'manoel@dj.emp.br', 'Financeiro', 'Contador', NULL),
(7, 'Laura', '123127', 'Laura', 'laura@dj.emp.br', 'RH', 'Pscï¿½loga', NULL),
(8, 'Debora', '123128', 'Débora', 'debora@dj.emp.br', 'RH', 'Gerente', NULL),
(9, 'Bruno', '123129', 'Bruno', 'bruno@dj.emp.br', 'Diretoria', 'CEO', NULL),
(10, 'Rodrigo', '123130', 'Rodrigo', 'rodrigo@dj.emp.br', 'Diretoria', 'CFO', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
