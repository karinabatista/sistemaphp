
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
	<head>
		<title>Cadastro de novo funcionário</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.onvisible.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
		<body class="no-sidebar">

		<!-- Header -->
			<div id="header">
				
				<!-- Nav -->
					<nav id="nav">
						<ul>
								<li><a href="">Ações</a>
									<ul>
										<li><a href="cadastro.php">Adicionar novo funcionário</a></li>
										<li><a href="listar.php">Listar Funcionários</a></li>
										<li><a href="listar.php">Editar Funcionários</a></li>
										<li><a href="listar.php">Excluir Funcionários</a></li>
									</ul>
								</li>
							<li><a href="logout.php">Sair</a></li>
						</ul>
					</nav>
			</div>

			<div class="wrapper style1">
				<div class="container">
					<article id="main" class="special">
						<header>
							<p>	<h2>Cadastro de novo funcionário</h2></p>
							<form action="cadastrando.php" method="post" name="cadastro" id="cadastro" ><center>
								<p><center>Defina o nome de usuário e senha para login no sistema</center></p>
								<label for="usuario">Usuário: </label><input type="text" name="nomeusuario" id="nome" />
								<label for="senha">Senha: </label><input type="password" " name="pass" id="senha" />
								
								
								<p><center>Digite os dados pessoais do funcionário.</center></p>
								<label for="nome">Nome: </label><input type="text" name="nome" id="nome" />
								<label for="email">E-mail: </label><input type="text" name="email" id="email" />
								<label for="setor">Setor: </label><input type="text" name="setor" id="setor" />
								<label for="cargo">Cargo: </label><input type="text" name="cargo" id="cargo" />
								<label for="foto">Deseja adicionar uma foto ao cadastro?</label>
								<div id="radio">
									<p><center>Sim <input type="radio" name="selecionar_arquivo id="sim" value="sim" onclick="document.formUpload.arquivo.disabled=false">
									Não<input type="radio" name="selecionar_arquivo id="nao" value="nao" checked="checked" onclick="document.formUpload.arquivo.disabled=true"></center></p>
								</div>
								<input type="file" name="foto" /><br /><br /><br /><p><center>
								<input type="hidden" name="arquivo" value="send">	
								<input type="submit" name="cadastrar" value="Cadastrar" /></p></center>
								
							</form></center>
						</header>
					 </article>	
				</div>
			</div>					
			<div align="center" class="copyright">
				<ul>
					<li>Aplicativo Web | By: <a href="#">Karina Batista</a> | TIM: (31)9119-4889 | VIVO: (31)71409-0222</li>
				</ul>
			</div>
	</body>
</html>