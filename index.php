<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//"
	"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
	<head>
		<title>Aplicativo Web</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/jquery.dropotron.min.js"></script>
		<script src="js/jquery.scrolly.min.js"></script>
		<script src="js/jquery.onvisible.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/skel-layers.min.js"></script>
		<script src="js/init.js"></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body class="homepage">

		<!-- Header -->
			<div id="header">
						
				<!-- Inner -->
					<div class="inner">
						<header>
							<h1><a href="index.php" id="logo">Aplicativo</a></h1>
							<hr />
							<p>Teste agora!</p>
						</header>
						<footer>
							<a href="#banner" class="button circled scrolly">Start</a>
						</footer>
					</div>
				
				<!-- Nav -->
					<nav id="nav">
						<ul>
							<li><a href="#contact">Contato</a></li>
						</ul>
					</nav>

			</div>
			
		<!-- Banner -->
			<section id="banner">
				<header>
					<h2>Oi! Seja <strong>bem vindo!</strong></h2>
					<p>
						Entre para descobrir as funcionalidades do aplicativo. (:
					</p><br />
					<form name="login" method="post" action="autenticacao.php">

       						<label><center>Usuario: </label><input type="text" id="nomeid" placeholder="Digite seu usuario" required="required" name="user" />  
						 	<label>Senha: </label><input type="password" id="passid" required="required" placeholder="******" name="password" /></center><br />
							<center><input type="submit" value="Entrar" /></center>
					</form>
				</header>
			</section>

		<!-- Footer -->
			<div id="footer">
				<div class="container">
					<div class="row">			
								<div class="copyright">
									<ul class="menu">
										<li>Entre em contato comigo nas redes sociais</li>
										<li>Design: <a href="#">Karina Batista</a></li>
										<li>TIM: (31)9119-4889</li>
										<li>VIVO: (31)71409-0222</li>
									</ul>
								</div>
							
						</div>
					
					</div>
				</div>
	</body>
</html>